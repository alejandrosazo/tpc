import java.util.Scanner;

public class MultiplicationGame2 {
	public static void main(String[] args) {
		int n;
		Scanner scn = new Scanner(System.in);
		String line = scn.nextLine();
		while(line != null) {
			n = Integer.parseInt(line);
			for( ; n > 18; n = (n + 17) / 18){}
			System.out.println( n <= 9 ? "Stan wins." : "Ollie wins.");
			line = scn.nextLine();
		}
	}
}