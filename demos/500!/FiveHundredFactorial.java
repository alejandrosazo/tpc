import java.math.BigInteger;
import java.util.Scanner;

public class FiveHundredFactorial {

    static BigInteger factorial[] = new BigInteger[10001];

    public static void main(String[] args) {
        factorial[0] = new BigInteger("1");
        factorial[1] = new BigInteger("1");
        llenar(factorial);
        Scanner lee = new Scanner(System.in);
        while (lee.hasNextInt()) {
            int n = lee.nextInt();
            System.out.println(n+"!");
            System.out.println(factorial[n]);
        }
    }

    public static void llenar(BigInteger fibo[]) {
        BigInteger A=new BigInteger("2");
        BigInteger I=new BigInteger("1");
        
        for (int i = 2; i <= 10000; i++) {
            fibo[i] =  fibo[i-1].multiply(A);
            A=A.add(I);
            
        }
    }

} 
